<?php

namespace App\Http\Controllers\Admin;

use App\Invitation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Jobs\SendEmailInvitationJob;

class DashboardController extends Controller
{
    protected $invitation;
    public function __construct()
    {
        $this->invitation = new Invitation();
    }

    public function index()
    {
        $invitations = $this->invitation->getAllWithStatus();
        return view('pages.admin.dashboard.index', compact('invitations'));
    }

    public function sendEmailInvitations(Request $request)
    {
        try {
            $email = isset($request->email) ? array_filter($request->email):[];
            if (count($email) > 0){
                dispatch(new SendEmailInvitationJob($request->email));
                $data = ['success' => 'success to send invitation!'];
            }else{
                $data = ['error' => 'there is no email received!'];
            }
            return redirect()
                ->back()
                ->with($data);
        }catch (\Exception $e){
            return redirect()
                ->back()
                ->with(['success' => 'whoops, looks like something wrong! :' .$e->getMessage()]);
        }
    }
}
