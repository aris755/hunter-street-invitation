<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    protected $email, $password;
    public function __construct(){

    }

    public function loginIndex(){
        return view('pages.admin.auth.login');
    }

    public function loginProcess(Request $request){
        try {
            $checkLogin = Auth::attempt(['email' => $request->email, 'password' => $request->password]);
            if ($checkLogin){
                $user = Auth::user();
                if ($user['role'] == 'admin'){
                    return redirect('/admin/dashboard');
                }else{
                    return redirect('/home');
                }
            }
            return redirect()
                ->back()
                ->withInput($request->all())
                ->with(['error' => "Your credentials doesnt match"]);
        }catch (\Exception $e){
            return redirect()
                ->back()
                ->withInput($request->all())
                ->with(['error' => 'Sorry, unable to handle this request:' . $e->getMessage()]);
        }
    }
}
