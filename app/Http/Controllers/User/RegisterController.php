<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\SubmitRegisterRequest;
use App\Invitation;
use App\Jobs\SendThanksRegisterMailToUserJob;
use App\Status;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RegisterController extends Controller
{
    protected $invitation, $status;
    public function __construct(){
        $this->invitation   = new Invitation();
        $this->status       = new Status();
    }

    public function index(Request $request) {
        try {
            $clientMail = 'not-registered@mail.com';
            $status     = false;
            $user       = collect([]);
            if (isset($request->code)){
                $email = decrypt($request->code);
                $user = $this->invitation->findByEmail($email);
                if ($user){
                    $clientMail = $user['email'];
                    if (isset($user['invitation_code'])){
                        $status = true;
                    }
                }
            }
            return view('pages.user.auth.register', compact('clientMail', 'status', 'user'));
        }catch (\Exception $e){
            return redirect('/event/register')
                ->withInput($request->all())
                ->with(['error' => 'Whoops, you like you trying to do something wrong!']);
        }
    }

    public function submitRegister(SubmitRegisterRequest $request) {
        try {
            $email          = $request->email;
            $findInvitation = $this->invitation->findByEmail($email);
            if (!$findInvitation){
                return redirect()
                    ->back()
                    ->withInput($request->all())
                    ->with(['error' => 'Sorry, you have no invitation for this event!']);
            }

            $findStatus = $this->status->findByInvitationId($findInvitation->id);
            if (!$findStatus){
                return redirect()
                    ->back()
                    ->withInput($request->all())
                    ->with(['error' => 'Please contact our customer support for your invitation information']);
            }

            DB::beginTransaction();
                $findInvitation->name = $request->name;
                $findInvitation->favorite_designers = json_encode($request->favorite_designer);
                $findInvitation->date_of_birth = $request->birth_date;
                $findInvitation->invitation_code = my_invitation_code($email);
                $findInvitation->gender = $request->gender;
                $findInvitation->save();
                $findStatus->status = 'user register successfully';
                $findStatus->save();
            DB::commit();

            $jobsMail = new SendThanksRegisterMailToUserJob($email);
            $jobsMail->delay(Carbon::now()->addMinutes(60));
            dispatch($jobsMail);
            return redirect()
                ->back()
                ->withInput($request->all())
                ->with(['success' => 'You are register on HUNTBAZAAR EVENT, always check your email for our updates!']);
        }catch (\Exception $e){
            DB::rollBack();
            return redirect()
                ->back()
                ->withInput($request->all())
                ->with(['error' => 'Sorry, looks like something wrong! Please contact our customer support for more irformation!']);
        }
    }
}
