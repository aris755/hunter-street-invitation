<?php

use Illuminate\Support\Facades\Mail;

if(! function_exists('my_default_response')){
    function my_default_response($message, $status = false){
        return ['status' => $status, 'message' => $message];
    }
}

if(! function_exists('my_designer_category')){
    function my_designer_category(){
        try {
            $goutte = new Goutte\Client(Symfony\Component\HttpClient\HttpClient::create(['timeout' => 60]));
            $getDesigner = $goutte->request('GET', 'https://www.huntstreet.com/designer');
            $designer = $getDesigner->filter('.dbRight > ul > li')->each(function ($node) {
                return $node->text();
            });
            return my_default_response($designer, true);
        }catch (\Exception $e){
            return my_default_response('Whoops, error: ' . $e->getMessage());
        }
    }
}

if(! function_exists('my_send_email_invitation')){
    function my_send_email_invitation($to_email, $url){
        try {
            $data = array('email'=> $to_email, 'url' => $url);
            Mail::send('mail.send-invitation', $data, function($message) use ($to_email) {
                $message->to($to_email)
                    ->subject('Selamat - Kamu Berkesempatan Mengikuti Event HUNTBAZAAR!');
                $message->from('sales@gmail.com','HUNTBAZAAR');
            });
            return my_default_response('success', true);
        }catch (\Exception $e){
            return my_default_response('Whoops, error: ' . $e->getMessage());
        }
    }
}

if(! function_exists('my_send_email_thanks')){
    function my_send_email_thanks($to_email){
        try {
            $data = array('email'=>"$to_email");
            Mail::send('mail.thanks-email-confirmation', $data, function($message) use ($to_email) {
                $message->to($to_email)
                    ->subject('Terimakasih telah ikut berpatisipasi dalam event HUNTBAZAAR!');
                $message->from('sales@admin.com','HUNTBAZAAR');
            });
            return my_default_response('success', true);
        }catch (\Exception $e){
            return my_default_response('Whoops, error: ' . $e->getMessage());
        }
    }
}

if(! function_exists('my_invitation_code')){
    function my_invitation_code($str){
        $time = strtotime(date('m/d/Y h:i:s a'));
        return base_convert(sha1($str.$time), 10, 36);
    }
}

if(! function_exists('my_invitation_link')){
    function my_invitation_link($str){
        return url('/event/register?code=').$str;
    }
}