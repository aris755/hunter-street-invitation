<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invitation extends Model
{
    protected $table = 'invitations';
    protected $guarded = [];

    public function findByEmail($email){
        return $this::with([])
            ->where('email', $email)
            ->orderBy('id','desc')
            ->first();
    }

    public function getAllWithStatus(){
        return $this::with(['status'])
        ->orderBy('id','desc')
        ->get();
    }

    public function status(){
        return $this->hasOne('App\Status','invitation_id', 'id')->orderBy('id','desc');
    }
}
