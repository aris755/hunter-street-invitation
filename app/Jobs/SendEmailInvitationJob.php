<?php

namespace App\Jobs;

use App\Invitation;
use App\Status;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class SendEmailInvitationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $email;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($email)
    {
        $this->email = $email;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $invitation = new Invitation();
        $status     = new Status();
        foreach ($this->email as $email) {
            if ($email!== null){
                $invitationLink = my_invitation_link(encrypt($email));
                $sendingEmail   = my_send_email_invitation($email, $invitationLink);
                DB::beginTransaction();
                if ($sendingEmail['status']){
                    $invitation->email = $email;
                    $invitation->invitation_link = $invitationLink;
                    $invitation->save();
                    $status->status = 'invitation mail sent successfully';
                    $status->invitation_id = $invitation->id;
                    $status->save();
                    DB::commit();
                }else{
                    DB::rollBack();
                }
            }
        }
    }
}
