<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $table = 'statuses';
    protected $guarded = [];

    public function findByInvitationId($id){
        return $this::with([])
            ->where('invitation_id', $id)
            ->orderBy('id','desc')
            ->first();
    }
}
