<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Invitation;
use Faker\Generator as Faker;

$factory->define(Invitation::class, function (Faker $faker) {
    $email = $faker->unique()->safeEmail;
    return [
        'email' => $email,
        'invitation_link' => my_invitation_link(encrypt($email)),
    ];
});
