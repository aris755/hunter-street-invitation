<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $output = new \Symfony\Component\Console\Output\ConsoleOutput();

        $this->call(
             InvitationSeeder::class,
             UserSeeder::class
         );
        $output->writeln(
"
         /
             *  Developer Message 
             *  From: Aris W.
             *  Don't use seeder email to register Invitation or sending mail because were using safe email
             *  The new laravel validation email can read it's real or fake mail
             *  Please try using manual sending from admin dashboard using your own mail
             *  All seeder default users password is 'password'
             *  Thanks for your attention
         /
         "
        );
    }
}
