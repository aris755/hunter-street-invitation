<?php

use Illuminate\Database\Seeder;

class InvitationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Invitation::class, 50)->create()->each(function ($status) {
            $status->status()->save(factory(App\Status::class)->make());
        });

    }
}
