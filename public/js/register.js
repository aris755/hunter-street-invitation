$(document).ready(function(){
    $(".multipleChosen").chosen({
        placeholder_text_multiple: "Choose Your Favorite Designer"
    });
    let date_input=$('input[name="birth_date"]');
    let container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
    let options={
        format: 'dd/mm/yyyy',
        container: container,
        todayHighlight: true,
        autoclose: true,
    };
    date_input.datepicker(options);
})

function makeTimer() {
    let endTime = new Date("20 November 2021 17:00:00");
    endTime = (Date.parse(endTime) / 1000);

    let now = new Date();
    now = (Date.parse(now) / 1000);

    let timeLeft = endTime - now;

    let days = Math.floor(timeLeft / 86400);
    let hours = Math.floor((timeLeft - (days * 86400)) / 3600);
    let minutes = Math.floor((timeLeft - (days * 86400) - (hours * 3600 )) / 60);
    let seconds = Math.floor((timeLeft - (days * 86400) - (hours * 3600) - (minutes * 60)));

    if (hours < "10") { hours = "0" + hours; }
    if (minutes < "10") { minutes = "0" + minutes; }
    if (seconds < "10") { seconds = "0" + seconds; }

    if (now > endTime){
        days = "00";
        hours = "00";
        minutes = "00";
        seconds= "00";
    }

    $("#days").html(days + "<span>Days</span>");
    $("#hours").html(hours + "<span>Hours</span>");
    $("#minutes").html(minutes + "<span>Minutes</span>");
    $("#seconds").html(seconds + "<span>Seconds</span>");

}
setInterval(function() { makeTimer(); }, 1000);
$('.btn-submit').click(function (){
    $(".p-info").each(function(){
        if ($(this).closest('div').find(":input").val() == ''){
            $(this).css("display","block")
        }else{
            $(this).css("display","none")
        }
    });
})