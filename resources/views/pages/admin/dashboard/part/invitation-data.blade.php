<div class="row">
    <div class="col-xl-12 col-md-12 mb-12">
        <div class="card shadow h-100 py-2">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Email</th>
                                <th>Name</th>
                                <th>Date Of Birth</th>
                                <th>Gender</th>
                                <th>Link</th>
                                <th>Code</th>
                                <th>Status</th>
                                <th>Updated At</th>
                            </tr>
                        </thead>
                        <tbody>
                        @if($invitations->isNotEmpty())
                            @foreach($invitations as $key => $invitation)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $invitation->email }}</td>
                                    <td>{{ $invitation->name }}</td>
                                    <td>{{ $invitation->date_of_birth }}</td>
                                    <td>{{ $invitation->gender }}</td>
                                    <td><a href="{{ $invitation->invitation_link }}" class="btn btn-sm btn-primary" target="_blank">preview</a> </td>
                                    <td>{{ $invitation->invitation_code }}</td>
                                    <td>{{ isset($invitation->status->status) ? $invitation->status->status : '' }}</td>
                                    <td>{{ $invitation->updated_at }}</td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>