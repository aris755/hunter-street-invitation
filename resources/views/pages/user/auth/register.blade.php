@extends('layouts.user-layouts')
@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css" type="text/css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.min.css" type="text/css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" type="text/css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
    <link rel="stylesheet" href="{{ asset('css/register.css') }}"/>
@endsection
@section('content')
    @include('layouts.session-message')
    <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
                <div class="col-lg-5 d-none d-lg-block bg-register-image">
                </div>
                <div class="col-lg-7">
                    <div class="row">
                        <div class="col-lg-12">
                            <h2 class="event-date">THE EVENT WILL START IN</h2>
                            <div id="timer">
                                <div class="timer" id="days"></div>
                                <div class="timer" id="hours"></div>
                                <div class="timer" id="minutes"></div>
                                <div class="timer" id="seconds"></div>
                            </div>
                        </div>
                    </div>
                    <div class="p-5">
                        @if($status && isset($user))
                            <h1 style="height: 35rem; text-align: center">Anda sudah terdaftar dengan kode registrasi: <b><u>{{ $user['invitation_code'] }}</u></b></h1>
                        @else
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4">Fill Your Registration!</h1>
                            </div>
                            <form class="user" id="form-register" method="post" action="{{ url('/event/submit-register') }}">
                                {{ csrf_field() }}
                                <div class="form-group row">
                                    <div class="col-sm-12 mb-3 mb-sm-0">
                                        <input type="text" class="form-control form-control-user" id="exampleFirstName"
                                               placeholder="Your Name" name="name" value="{{ old('name') }}" required>
                                        <p class="text-sm-left small text-danger p-info">
                                            *required
                                        </p>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12 mb-3 mb-sm-0">
                                        <select class="multipleChosen form-control form-control-user" id="selectDesigner"  multiple="true" name="favorite_designer[]" required>
                                            @if(my_designer_category()['status'])
                                                @foreach(my_designer_category()['message'] as $designer)
                                                    <option value="{{ $designer }}">{{ $designer }}</option>
                                                @endforeach
                                            @else
                                                <option value="">-</option>
                                            @endif
                                        </select>
                                        <p class="text-sm-left small text-danger p-info" style="display: block!important;">
                                            * choose the designer
                                        </p>
                                        <p class="text-muted text-sm-left small  p-info" style="display: block!important;">
                                            For the reference, you can see at: <a href="{{ url('/') }}">{{ url('/') }}</a> or <a href="https://www.huntstreet.com/designer">https://www.huntstreet.com/designer</a>
                                        </p>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12 mb-3 mb-sm-0">
                                        <input class="form-control form-control-user" id="birth_date" name="birth_date"
                                               placeholder="Your Birth Date, Ex: DD/MM/YYY" type="text" required value="{{ old('birth_date') }}"/>
                                        <p class="text-sm-left small text-danger p-info">
                                            * required
                                        </p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control form-control-user" id="exampleInputEmail"
                                           placeholder="Email Address" name="email" required value="{{ $clientMail }}" readonly>
                                    <p class="text-sm-left small text-info p-info" style="display: block">
                                        * autofill
                                    </p>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12 mb-3 mb-sm-0">
                                        <select class="form-select form-control form-control-user select-gender"
                                                aria-label="Default select example" required name="gender">
                                            <option value="">-- Gender --</option>
                                            <option value="male">Male</option>
                                            <option value="female">Female</option>
                                        </select>
                                        <p class="text-sm-left small text-danger p-info">
                                            * required
                                        </p>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary btn-user btn-block btn-submit">
                                    Register Event
                                </button>
                                <hr>
                                <a href="#" class="btn btn-google btn-user btn-block">
                                    <i class="fab fa-google fa-fw"></i> Register with Google
                                </a>
                                <a href="#" class="btn btn-facebook btn-user btn-block">
                                    <i class="fab fa-facebook-f fa-fw"></i> Register with Facebook
                                </a>
                            </form>
                            <hr>
                            <div class="text-center">
                                <a class="small" href="#">Forgot Password?</a>
                            </div>
                            <div class="text-center">
                                <a class="small" href="#">Already have an account? Login!</a>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
    <script src="{{ asset('js/register.js') }}"></script>
@endsection