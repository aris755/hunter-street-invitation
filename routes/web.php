<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'User\IndexController@index');
Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function (){
    Route::group(['middleware' => 'admin.login'], function (){
        Route::get('/dashboard', 'DashboardController@index');
        Route::post('/send-invitations', 'DashboardController@sendEmailInvitations');
        Route::get('/invitation', 'DashboardController@index');
        Route::get('/invitation', 'DashboardController@index');
    });
    Route::get('/login', 'LoginController@loginIndex');
    Route::post('/login-process', 'LoginController@loginProcess');
});

Route::group(['prefix' => 'event', 'namespace' => 'User'], function (){
    Route::get('/register', 'RegisterController@index');
    Route::post('/submit-register', 'RegisterController@submitRegister');
});

Auth::routes();
Route::get('/register', function (){
    return redirect('/event/register');
});
Route::get('/login', function (){
    return redirect('/admin/login');
});


Route::get('/home', 'HomeController@index')->name('home');
